from django.views.generic import ListView, CreateView
from .models import Entry

class EntryList(ListView):
    model = Entry

entry_list = EntryList.as_view()

class EntryAdd(CreateView):
    model = Entry
    fields = ["url"]

entry_add = EntryAdd.as_view()
