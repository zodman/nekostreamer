from __future__ import unicode_literals

from django.db import models
from account.models import Account


class Entry(models.Model):
    account = models.ForeignKey(Account)
    url = models.URLField()

    def __unicode__(self):
        return self.url
