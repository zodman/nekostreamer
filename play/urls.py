from django.conf.urls import  url
from .views import entry_list, entry_add

urlpatterns = [
    url(r"add$", entry_add, name="add"),
    url(r"$", entry_list, name="list"),
]

